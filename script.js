const gameBoard = document.getElementById("gameBoard");
const winSpan = document.getElementById("winSpan");
const map1 = [
    "TTWWWWWT",
    "WWW   WT",
    "WOSB  WT",
    "WWW BOWT",
    "WOWWB WT",
    "W W O WW",
    "WB XBBOW",
    "W   O  W",
    "WWWWWWWW"
];

const map2 = [  
    "    WWWWW          ",  
    "    W   W          ",  
    "    WB  W          ",  
    "  WWW  BWW         ",  
    "  W  B B W         ",  
    "WWW W WW W   WWWWWW",  
    "W   W WW WWWWW  OOW",  
    "W B  B          OOW",  
    "WWWWW WWW WSWW  OOW",  
    "    W     WWWWWWWWW",  
    "    WWWWWWW        "  
]; 

const map3 = [
    "   WWWW    ",
    "WWWW  W    ",
    "W     WWWW ",
    "W B W  O WW",
    "W  W   O  W",
    "WW WBBWO  W",
    "WW    WWWWW",
    "W S WWW    ",
    "W   W      ",
    "WWWWW      "
];   //level copied from http://borgar.net/programs/sokoban/#Sasquatch%20IV

const map4 = [
    "WWWWWWWWWWWW  ",
    "WOO  W     WWW",
    "WOO  W B  B  W",
    "WOO  WBWWWW  W",
    "WOO    S WW  W",
    "WOO  W W  B WW",
    "WWWWWW WWB B W",
    "  W B  B B B W",
    "  W    W     W",
    "  WWWWWWWWWWWW"
]; //level copied from https://webdocs.cs.ualberta.ca/~games/Sokoban/

const map5 = [
    "WWWWWWWWWWWWWWWWWWWW",
    "WOOW    W          W",
    "WOB  B  WBB  BWW BWW",
    "WOBW  WWW  WW WW   W",
    "W  W B W  BB   B   W",
    "W WWW  W W  WB  WWWW",
    "W  WW W B   WS W   W",
    "W B    B  WWOWW  B W",
    "W  W BW BW B     WWW",
    "W  W  W  W   WWW   W",
    "W  WWWWWWWW W      W",
    "W           W  WOWOW",
    "WWBWWWWWWWWBW   OOOW",
    "W    OX  W    WWOWOW",
    "W OXOOOX   B  OOOOOW",
    "WWWWWWWWWWWWWWWWWWWW"
]; //level copied from https://webdocs.cs.ualberta.ca/~games/Sokoban/

let dataModel = [];
function createMap(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        const rowModel = [];
        const rowView = document.createElement("div");
        rowView.className = "row";
        rowView.id = ("row" + rowIndex);
        gameBoard.appendChild(rowView);
        dataModel.push(rowModel);
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            const cell = document.createElement("div");
            cell.className = "cell";
            cell.id = ("cell" + rowIndex + "-" + columnIndex);
            cell.dataset.type = (map[rowIndex][columnIndex])
            rowView.appendChild(cell);
            rowModel.push(cell);
            if (dataModel[rowIndex][columnIndex].dataset.type === "W") { dataModel[rowIndex][columnIndex].appendChild(createWall()) }
            if (dataModel[rowIndex][columnIndex].dataset.type === "X") { dataModel[rowIndex][columnIndex].appendChild(createFilledStorage()) }
            if (dataModel[rowIndex][columnIndex].dataset.type === "S") { dataModel[rowIndex][columnIndex].appendChild(createPlayer()) }
            if (dataModel[rowIndex][columnIndex].dataset.type === "B") { dataModel[rowIndex][columnIndex].appendChild(createbox()) }
        }
    }
}

function createWall() {
    let box = document.createElement("div");
    box.className = "wall"
    return box;
}

function createPlayer(){
    let player = document.createElement("div");
    player.id = "player";
    return player;
}

function createbox() {
    let box = document.createElement("div")
    box.className = "box"
    return box
}

function createFilledStorage() {
    let box = document.createElement("div")
    box.className = "fullStorage"
    box.appendChild(createbox())
    return box
}

function findPlayerCurrentColumn(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            let currentCell = map[rowIndex][columnIndex]
            if (currentCell.lastElementChild === player) { return (columnIndex) }
        }
    }
}

function findPlayerCurrentRow(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            let currentCell = map[rowIndex][columnIndex]
            if (currentCell.lastElementChild === player) { return (rowIndex) }
        }
    }
}

function movePlayer(map, direction) {
    let currentColumn = findPlayerCurrentColumn(map);
    let currentRow = findPlayerCurrentRow(map);
    if ((direction === "ArrowUp") && (map[currentRow - 1][currentColumn].childElementCount === 0)) {
        map[currentRow - 1][currentColumn].appendChild(player);
    }
    if ((direction === "ArrowDown") && (map[currentRow + 1][currentColumn].childElementCount === 0)) {
        map[currentRow + 1][currentColumn].appendChild(player);
    }
    if ((direction === "ArrowLeft") && (map[currentRow][currentColumn - 1].childElementCount === 0)) {
        map[currentRow][currentColumn - 1].appendChild(player);
    }
    if ((direction === "ArrowRight") && (map[currentRow][currentColumn + 1].childElementCount === 0)) {
        map[currentRow][currentColumn + 1].appendChild(player);
    }
}

function moveBox(map, direction, box) {
    let currentColumn = findPlayerCurrentColumn(map);
    let currentRow = findPlayerCurrentRow(map);
    if ((direction === "ArrowUp") && (map[currentRow - 2][currentColumn].childElementCount === 0)) {
        if (checkForWall(dataModel, "ArrowUp", 2) === "illegal move") { return "illegal move" }
        map[currentRow - 2][currentColumn].appendChild(box);
    }
    if ((direction === "ArrowDown") && (map[currentRow + 2][currentColumn].childElementCount === 0)) {
        if (checkForWall(dataModel, "ArrowDown", 2) === "illegal move") { return "illegal move" }
        let targetbox = map[currentRow + 2][currentColumn];
        targetbox.appendChild(box);
    }
    if ((direction === "ArrowLeft") && (map[currentRow][currentColumn - 2].childElementCount === 0)) {
        if (checkForWall(dataModel, "ArrowLeft", 2) === "illegal move") { return "illegal move" }
        map[currentRow][currentColumn - 2].appendChild(box);
    }
    if ((direction === "ArrowRight") && (map[currentRow][currentColumn + 2].childElementCount === 0)) {
        if (checkForWall(dataModel, "ArrowRight", 2) === "illegal move") { return "illegal move" }
        map[currentRow][currentColumn + 2].appendChild(box);
    }
}

function checkForWall(map, direction, distance) {
    let currentColumn = findPlayerCurrentColumn(map);
    let currentRow = findPlayerCurrentRow(map);
    if ((direction === "ArrowUp") && (map[currentRow - distance][currentColumn].dataset.type === "W")) { return ("illegal move") }
    else if ((direction === "ArrowDown") && (map[currentRow + distance][currentColumn].dataset.type === "W")) { return ("illegal move") }
    else if ((direction === "ArrowLeft") && (map[currentRow][currentColumn - distance].dataset.type === "W")) { return ("illegal move") }
    else if ((direction === "ArrowRight") && (map[currentRow][currentColumn + distance].dataset.type === "W")) { return ("illegal move") }
}

function checkForBox(map, direction) {
    let currentColumn = findPlayerCurrentColumn(map);
    let currentRow = findPlayerCurrentRow(map);
    if ((direction === "ArrowUp") && (map[currentRow - 1][currentColumn].lastElementChild !== null)) {
        let box = map[currentRow - 1][currentColumn].lastElementChild;
        moveBox(dataModel, "ArrowUp", box)
    }
    else if ((direction === "ArrowDown") && (map[currentRow + 1][currentColumn].lastElementChild !== null)) {
        let box = map[currentRow + 1][currentColumn].lastElementChild;
        moveBox(dataModel, "ArrowDown", box)
    }
    else if ((direction === "ArrowLeft") && (map[currentRow][currentColumn - 1].lastElementChild !== null)) {
        let box = map[currentRow][currentColumn - 1].lastElementChild;
        moveBox(dataModel, "ArrowLeft", box)
    }
    else if ((direction === "ArrowRight") && (map[currentRow][currentColumn + 1].lastElementChild !== null)) {
        let box = map[currentRow][currentColumn + 1].lastElementChild;
        moveBox(dataModel, "ArrowRight", box)
    }
}

function checkStorage(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            if ((map[rowIndex][columnIndex].dataset.type === "O") && (map[rowIndex][columnIndex].childElementCount === 1)) {
                map[rowIndex][columnIndex].style.border = "2px dashed yellowgreen";
            } else if ((map[rowIndex][columnIndex].dataset.type === "O") && (map[rowIndex][columnIndex].childElementCount === 0)) {
                map[rowIndex][columnIndex].style.border = "2px solid purple";
            }
            if ((map[rowIndex][columnIndex].dataset.type === "X") && (map[rowIndex][columnIndex].childElementCount === 0)) {
                map[rowIndex][columnIndex].style.border = "2px solid purple";
            } else if ((map[rowIndex][columnIndex].dataset.type === "X") && (map[rowIndex][columnIndex].childElementCount === 1)) {
                map[rowIndex][columnIndex].style.border = "2px dashed yellowgreen";
            }
        }
    }
}

function checkForWin(map) {
    for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
        for (let columnIndex = 0; columnIndex < map[rowIndex].length; columnIndex++) {
            if ((map[rowIndex][columnIndex].dataset.type === "O") && (map[rowIndex][columnIndex].childElementCount !== 1)) {
            return false
            } else if ((map[rowIndex][columnIndex].dataset.type === "X") && (map[rowIndex][columnIndex].childElementCount !== 1)) {
            return false
            }
        }
    }
}

document.addEventListener("keydown", function (event) {
    let direction = event.key;
    if(event.key === 'r'){location.reload();}
    if (checkForWall(dataModel, direction, 1) === "illegal move") { return }
        checkForBox(dataModel, direction);
        movePlayer(dataModel, direction);
        checkStorage(dataModel);
    if      (checkForWin(dataModel) !== false){winSpan.innerText = "You Win"}
    else if (checkForWin(dataModel) === false){winSpan.innerText = " "}
});

function changeMap(map){
    gameBoard.innerHTML = "";
    winSpan.innerText = ""
    dataModel.length = 0;
    createMap(map);
}
